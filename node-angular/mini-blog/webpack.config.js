var webpack = require('webpack');
var path    = require('path');

module.exports = {
  entry    : {
    client: path.join(__dirname, 'src/client/client.js'),
    admin: path.join(__dirname, 'src/admin/admin.js'),
  },
  output   : {
    path     : path.resolve(__dirname, 'public/js/'),
    filename : '[name].js'
  },
  devtool: 'source-map',

  module: {
    loaders: [
      {
        test    : /\.js$/,
        exclude : [/node_modules/, /public/],
        loader  : 'babel',
        query   : {
          presets: ['es2015'],
          plugins: [
            'add-module-exports'
          ]
        },
      },
      {
        test    : /\.css$/,
        loader  : "style-loader!css-loader!autoprefixer-loader",
        exclude : [/node_modules/, /public/]
      },
    ]
  },

  devServer: {
    proxy: {
      '*': {
        target: 'http://127.0.0.1:3001/',
        secure: false
      }
    }
  },
  proxy: {
    '*': {
      target: 'http://127.0.0.1:3000/',
      secure: false
    }
  }
}