var router = require('express').Router();

const user = {
  username: 'admin',
  password: 'admin'
};

router.get('/admin/:name?', (req, res) => {
  if (!req.session.user) {
    return res.render('admin/login');
  }

  res.render('admin/index');
});

router.post('/admin/login', (req, res) => {
  let User = req.body;

  if (User.username == user.username && User.password == user.password) {
    req.session.user = User;
  }

  res.redirect('/admin');
});

router.post('/admin/logout', (req, res) => {
  req.session.user = undefined;

  res.render('admin/index');
});

module.exports = router;