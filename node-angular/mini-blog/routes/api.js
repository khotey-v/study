var router = require('express').Router();

var BookmarkTheme = require('../models/bookmark-theme');
var Bookmark = require('../models/bookmark');
var sortBookmarks = require('../helpers/sort-bookmarks');


router.get('/api/bookmarks-theme/', (req, res, next) => {

  BookmarkTheme.find({}, (err, data) => {
    if (err) return next(err);

    res.json(data);
  });
});

// router.get('/api/bookmark-theme/:name', (req, res, next) => {

//   async.waterfall([
//     function getBookmarkTheme(callback) {
//       BookmarkTheme.findOne({name: req.params.name}, (err, bookmarkTheme) => {
//         if (err) return next(err);

//         if (!bookmarkTheme) {
//           return res.status(404).json('404. Bookmark not found');
//         }

//         callback(null, bookmarkTheme);
//       });
//     },

//     function getBookmarks(bookmarkTheme, callback) {
//       Bookmark.find({themeId: bookmarkTheme._id}, (err, bookmarks) => {
//         if (err) return next(err);

//         return sortBookmarks(bookmarks, res);
//       });
//     }
//   ])
// });

// router.post('/api/bookmarks-theme/', (req, res, next) => {
//   BookmarkTheme.create(req.body, (err, bookmarkTheme) => {
//     if (err) return next(err);

//     res.json(bookmarkTheme);
//   });
// });

// router.post('/api/bookmark/', (req, res, next) => {

//   async.waterfall([
//     function getBookmarkTheme(callback) {
//       BookmarkTheme.findOne({name: req.body.bookmarkTheme}, (err, bookmarkTheme) => {
//         if (err) return next(err);

//         let bookmarkThemeId = bookmarkTheme._id;
//         callback(null, bookmarkThemeId);
//       });
//     },

//     function createBookmarkByTheme(themeId, callback) {
//       Bookmark.create({themeId: themeId, name: req.body.name, href: req.body.href}, (err, bookmark) => {
//         if (err) return next(err);

//         res.json(bookmark);
//       });
//     }
//   ])

// });

module.exports = router;