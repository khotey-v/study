export default class appDir {

  constructor() {

    this.scope = true;
    this.restrict = 'E';
    this.controllerAs = 'appCtrl';
    this.controller = appDirController;
  }
}

class appDirController {

  constructor(Pages) {

    Pages
      .getData()
      .then((data) => {
        this.data = data.data;
      });

    this.selected = 'main';
  }
}

appDirController.$inject = ['Pages'];