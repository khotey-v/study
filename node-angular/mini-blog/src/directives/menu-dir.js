export default class MenuDir {

  constructor() {
    this.scope = { menu: '=', selected: '=' };
    this.restrict = 'E';
    this.replace = true;
    this.controllerAs = 'menuCtrl';
    this.controller = MenuDirCtrl;
    this.bindToController = true;
    this.templateUrl = '/templates/directives/menu-template.html'
  }
}

class MenuDirCtrl {

  selectMenu(menu) {
    this.selected = menu;
  }

  isSelected(menu) {
    return menu === this.selected;
  }
}