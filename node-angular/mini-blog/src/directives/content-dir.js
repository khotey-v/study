export default class ContentDir {

  constructor() {
    this.scope = {text: '='},
    this.restrict = 'E';
    this.replace = true;
    this.controllerAs = 'contentCtrl';
    this.templateUrl = '/templates/directives/text-template.html'
  }
}