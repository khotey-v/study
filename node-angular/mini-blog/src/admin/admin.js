import clientCss from '../client/media/css/style.css';

import angular from 'angular';
import angularRoute from 'angular-ui-router';
import ngSanitize from 'angular-sanitize';
import uiBootstrap from 'angular-ui-bootstrap';

import config from './config';
import AdminData from './services/admin-data';
import BookmarkTheme from '../client/services/bookmarks-theme';
import Bookmark from '../client/services/bookmark';
import Animation from '../client/services/animation';
import Auth from './services/auth';
import MenuDir from './directives/menu-dir';

var app = angular
  .module('MiniBlogAdmin', [
    ngSanitize,
    angularRoute,
    uiBootstrap
]);

app.config(config);

app
  .directive('menuDir', () => new MenuDir)
  .service('AdminData', AdminData)
  .service('BookmarkTheme', BookmarkTheme)
  .service('Bookmark', Bookmark)
  .service('Auth', Auth)
  .service('Animation', Animation);
