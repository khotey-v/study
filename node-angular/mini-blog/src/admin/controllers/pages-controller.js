export default class PagesController {

  constructor($stateParams, $location, Pages) {
    this.selected = $stateParams.page;
    this.Pages = Pages;
    Pages
      .getPages()
      .then((data) => {
        this.data = data.data;
      });
  }

  delete(index) {
    this.Pages
      .delete(index)
      .then((data) => {
        delete this.data[index];
      });
  }

  update(index) {
    // update
  }
}

PagesController.$inject = ['$stateParams', '$location', 'Pages'];