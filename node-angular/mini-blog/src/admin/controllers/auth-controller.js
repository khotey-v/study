export default class AuthController {

  constructor($stateParams, Auth) {
    let action = $stateParams.action;
    this.Auth = Auth;

    if (action === 'logout') {
      this.logout();
    }
  }

  logout() {
    this.Auth
      .logout()
      .then((data) => {
        location.pathname = '/admin';
      });
  }
}

AuthController.$inject = ['$stateParams','Auth'];