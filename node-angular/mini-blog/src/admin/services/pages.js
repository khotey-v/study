export default class Pages {

  constructor($http) {
    this.$http = $http;
  }

  getContent(menu) {
    return this.$http.get('/adminApi/page/' + menu);
  }

  getMenu() {
    return this.$http.get('/adminApi/pages/menu');
  }

  getPages() {
    return this.$http.get('/adminApi/pages');
  }

  delete(menu) {
    return this.$http.delete('/adminApi/page/' + menu);
  }
}

Pages.$inject = ['$http'];

// РАЗДЕЛИЛЬ  pages на админский и клиентский проверить api на сервере