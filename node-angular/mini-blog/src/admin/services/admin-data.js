export default class AdminData {

  constructor($http) {
    this.$http = $http;
  }

  getMenu() {
    return this.$http.get('/adminApi/menu');
  }
}

AdminData.$inject = ['$http'];