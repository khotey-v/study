export default class Auth {

  constructor($http) {
    this.$http = $http;
  }

  logout() {
    return this.$http.post('/admin/logout');
  }
}

Auth.$inject = ['$http'];

// РАЗДЕЛИЛЬ  pages на админский и клиентский проверить api на сервере