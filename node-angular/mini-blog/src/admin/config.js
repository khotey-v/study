import BookmarksController from './controllers/bokmarks-controller';
import AuthController from './controllers/auth-controller';

const config = ($stateProvider, $urlRouterProvider, $locationProvider) => {

  $locationProvider.html5Mode({
    enabled: true,
    requireBase: false
  });

  $urlRouterProvider.otherwise('/admin');

  $stateProvider
    .state('bookmarks', {
      url: '/admin/bookmarks',
      templateUrl: '/templates/admin/directives/bookmarks-template.html',
      controller: BookmarksController,
      controllerAs: 'BookmarksCtrl'
  });
};

config.$inject = ['$stateProvider', '$urlRouterProvider', '$locationProvider'];

export default config;