export default class MenuDir {

  constructor() {
    this.scope = true;
    this.restrict = 'E';
    this.replace = true;
    this.controllerAs = 'menuCtrl';
    this.controller = MenuDirCtrl;
    this.bindToController = true;
    this.templateUrl = '/templates/admin/directives/menu-template.html'
  }
}

class MenuDirCtrl {

  constructor($location, AdminData) {
    this.$location = $location;

    AdminData
      .getMenu()
      .then((data) => {
        this.menu = data.data;
      })
  }

  isSelected(menu) {
    let page = this.$location.url().split('/')[2];

    return menu === page;
  }
}

MenuDirCtrl.$inject = ['$location', 'AdminData'];