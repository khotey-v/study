import clientCss from './media/css/style.css';

import angular from 'angular';
import angularRoute from 'angular-ui-router';
import ngSanitize from 'angular-sanitize';

import config from './config';
import menuDir from './directives/menu-dir';
import contentDir from './directives/content-dir';
import HeaderDir from './directives/header-dir';
import BookmarkTheme from './services/bookmarks-theme';
import Bookmark from './services/bookmark';
import Animation from './services/animation';

var app = angular
  .module('MiniBlog', [
    ngSanitize,
    angularRoute
  ]);

app.config(config);

app
  .directive('menuDir', () => new menuDir)
  .directive('contentDir', () => new contentDir)
  .directive('headerDir', () => new HeaderDir)
  .service('BookmarkTheme', BookmarkTheme)
  .service('Bookmark', Bookmark)
  .service('Animation', Animation);
