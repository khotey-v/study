export default class ContentDir {

  constructor() {
    this.scope = {text: '='},
    this.restrict = 'E';
    this.replace = true;
    this.templateUrl = '/templates/client/directives/text-template.html';
  }
}