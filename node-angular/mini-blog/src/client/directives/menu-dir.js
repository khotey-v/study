export default class MenuDir {

  constructor() {
    this.scope = true;
    this.restrict = 'E';
    this.replace = true;
    this.controllerAs = 'menuCtrl';
    this.controller = MenuDirCtrl;
    this.bindToController = true;
    this.templateUrl = '/templates/client/directives/menu-template.html';
  }
}

class MenuDirCtrl {

  constructor($location, BookmarkTheme, Animation) {
    this.$location = $location;
    this.Animation = Animation;

    BookmarkTheme
      .getBookmarkTheme()
      .then((data) => {
        this.bookmarkThemes = data.data;
      })
  }

  isSelected(menu) {
    let page = this.$location.url().split('/')[2];

    return menu === page;
  }

  refreshContent() {
    this.Animation.hideContent('#main-content');
  }
}


MenuDirCtrl.$inject = ['$location', 'BookmarkTheme', 'Animation'];