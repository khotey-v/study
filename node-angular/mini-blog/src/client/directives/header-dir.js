export default class HeaderDir {

  constructor() {
    this.restrict = 'E';
    this.replace = true;
    this.templateUrl = '/templates/client/directives/header-template.html';
  }
}