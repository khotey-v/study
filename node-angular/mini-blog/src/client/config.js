import BookmarkController from './controllers/bookmark-controller';

const config = ($stateProvider, $urlRouterProvider, $locationProvider) => {

  $locationProvider.html5Mode({
    enabled: true,
    requireBase: false
  });

  $urlRouterProvider.otherwise('/');

  $stateProvider
    .state('bookmark', {
      url: '/bookmark-theme/:name',
      templateUrl: '/templates/client/directives/bookmarks-template.html',
      controller: BookmarkController,
      controllerAs: 'BookmarkCtrl'
  });

  $stateProvider
    .state('bio', {
      url: '/',
      templateUrl: '/templates/client/directives/bio-template.html',
  });
};

config.$inject = ['$stateProvider', '$urlRouterProvider', '$locationProvider'];

export default config;