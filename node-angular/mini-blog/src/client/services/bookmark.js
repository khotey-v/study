export default class Bookmark {

  constructor($http) {
    this.$http = $http;
  }


  getBookmarks(name) {
    return this.$http.get('/api/bookmark-theme/' + name);
  }
}

Bookmark.$inject = ['$http'];

// РАЗДЕЛИЛЬ  pages на админский и клиентский проверить api на сервере