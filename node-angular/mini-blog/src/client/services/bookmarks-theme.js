export default class BookmarkTheme {

  constructor($http) {
    this.$http = $http;
  }


  getBookmarkTheme() {
    return this.$http.get('/api/bookmarks-theme/');
  }
}

BookmarkTheme.$inject = ['$http'];

// РАЗДЕЛИЛЬ  pages на админский и клиентский проверить api на сервере