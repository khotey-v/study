import async from 'async';

export default class BookmarkController {

  constructor($stateParams, $location, Bookmark, Animation) {
    this.selected = $stateParams.name;

    if (!this.selected) {
      return;
    }

    Bookmark
      .getBookmarks(this.selected)
      .then((data) => {
        this.bookmarks = data.data;
        this.isShow = !Array.isArray(this.bookmarks);
        Animation.showContent('#main-content');
      }, (err) => {
        this.isErr = true;
        this.err = err.data;
      });
  }
}

BookmarkController.$inject = ['$stateParams', '$location', 'Bookmark', 'Animation'];