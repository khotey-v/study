const async = require('async');

module.exports = function(bookmarks, res) {

  if (!bookmarks.length) {
    return res.json([]);
  }

  async.parallel([
    function sortNotRead(callback) {
      let notReadBookmarks = bookmarks.filter((bookmark) => {
        if (!bookmark.isReaded) {
          return true;
        }
      });

      callback(null, { notRead: notReadBookmarks });
    },

    function sortRead(callback) {
      let readBookmarks = bookmarks.filter((bookmark) => {
        if (bookmark.isReaded) {
          return true;
        }
      });

      callback(null, { read: readBookmarks });
    }
  ], function (err, result) {

    let bookmarks = {};

    result.forEach((el, index, array) => {
      const arrayLength = array.length - 1;

      if (el.hasOwnProperty('read')) {
        bookmarks.read = el.read;
      }

      if (el.hasOwnProperty('notRead')) {
        bookmarks.notRead = el.notRead;
      }

      if (index === arrayLength) {
        res.json(bookmarks);
      }
    });
  });
}