let mongoose = require('mongoose');

const Schema = mongoose.Schema;

const User = new Schema({
  login: { type: String, unique: true, lowercase: true, index: true },
  password: String
});

module.exports = mongoose.model('User', User);