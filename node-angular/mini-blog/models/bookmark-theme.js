let mongoose = require('mongoose');

const Schema = mongoose.Schema;

const BookmarkTheme = new Schema({
  name: { type: String, unique: true, lowercase: true, index: true }
});

module.exports = mongoose.model('BookmarkTheme', BookmarkTheme);