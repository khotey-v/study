let mongoose = require('mongoose');

const Schema = mongoose.Schema;

const Bookmark = new Schema({
  name: { type: String, unique: true, lowercase: true, index: true },
  isReaded: { type: Boolean, default: false },
  href: { type: String },
  themeId: { type: Schema.Types.ObjectId, ref: 'BookmarkTheme' },
  createdAt: { type: Date, default: Date.now }
});

module.exports = mongoose.model('Bookmark', Bookmark);