var express = require('express');
var session = require('express-session');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var ejsMate = require('ejs-mate');
var mongoose = require('mongoose');

var config = require('./config/secret');
var mainRoute = require('./routes/main');
var apiRoute = require('./routes/api');
var adminRoute = require('./routes/admin')
var adminApiRoute = require('./routes/admin-api')

const app = express();

app.listen(config.port, (err) => {
  if (err) throw err;

  console.log(`server listening port: ${config.port}`)
});

mongoose.connect(config.database, function (err) {
  if (err) {
    console.error('Can\'t connect to mongodb');

    throw err;
  }

  console.log('Database connected success !');
});

app.engine('ejs', ejsMate);
app.set('view engine', 'ejs');
app.use(express.static('public'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(session({
  resave: true,
  saveUninitialized: true,
  secret: config.secretKey,
  // store: new MongoStore({url: secret.database, autoReconect: true})
}));

app.use(apiRoute);
app.use(adminApiRoute);

app.use(adminRoute);
app.use(mainRoute);

app.use(function errorHandler(err, req, res, next) {
  console.error(err);

  res
    .status(500)
    .json('Server Error. Please try again later.');
});
