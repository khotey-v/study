import express from 'express';
import request from 'request';

const router = express.Router();

router.get('*', async (req, res) => {
  const responce = await getBodyFromRequest();

  res.end(responce);
});

function getBodyFromRequest() {
  return new Promise((resolve, reject) => {
    request('http://www.google.com', (error, response, body) => {
      resolve(body);
    });
  });
}

export default router;
