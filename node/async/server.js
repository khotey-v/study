import express from 'express';
import session from 'express-session';
import cookieParser from 'cookie-parser';
import bodyParser from 'body-parser';
import ejsMate from 'ejs-mate';
import mongoose from 'mongoose';
import flash from 'express-flash';

import config from './config';
import mainRoute from './routes/main';
import errorLogger from './middlewares/errorLogger';

const app = express();

app.listen(config.port, (err) => {
  if (err) throw err;

  console.info(`Server's PORT:${config.port}`);
});

// REMOVE
if (config.database) {
  mongoose.connect(config.database, (err) => {
    if (err) throw err;

    console.info(`Success connect to mongodb:${config.database}`);
  });
}

app.engine('ejs', ejsMate);
app.set('view engine', 'ejs');
app.use(express.static('public'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(session({
  resave: true,
  saveUninitialized: true,
  secret: config.secretKey
}));
app.use(flash());

app.use(mainRoute);

// Error handler

app.use(errorLogger);