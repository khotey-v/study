import mongoose from 'mongoose';
import bcrypt from 'bcrypt-nodejs';
import async from 'async';

const Schema = mongoose.Schema;

const User = new Schema({
  login: { type: String, unique: true, lowercase: true, index: true},
  password: { type: String }
});

User.pre('save', function (next) {
  const user = this;
  if (!user.isModified('password')) {
    return next();
  }

  async.waterfall([
    function genSalt(callback) {
      bcrypt.genSalt(10, (err, salt) => {
        if (err) return next(err);

        callback(null, salt);
      })
    },

    function cryptPassword(salt, callback) {
      bcrypt.hash(user.password, salt, null, (err, hash) => {
        if (err) return next(err);

        user.password = hash;
        next();
      })
    }
  ]);
});

User.methods.comparePassword = function (password) {
  return bcrypt.compareSync(password, this.password);
};

export default mongoose.model('User', User);