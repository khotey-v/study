import express from 'express';
import session from 'express-session';
import cookieParser from 'cookie-parser';
import bodyParser from 'body-parser';
import ejsMate from 'ejs-mate';
import mongoose from 'mongoose';
import flash from 'express-flash';
import validator from 'express-validator';

import config from './config';
import mainRoute from './routes/main';
import adminRoute from './routes/admin';
import userRoute from './routes/user';
import errorLogger from './middlewares/errorLogger';
import customValidators from './helper/custom-validator';
import redirectBack from './middlewares/redirect-back-middleware';
import currentUrl from './middlewares/current-url-middleware';
import authMiddleware from './middlewares/auth-middleware';

const app = express();

app.listen(config.port, (err) => {
  if (err) throw err;

  console.info(`Server's PORT:${config.port}`);
});


mongoose.connect(config.database, (err) => {
  if (err) throw err;

  console.info(`Success connect to mongodb:${config.database}`);
});


app.engine('ejs', ejsMate);
app.set('view engine', 'ejs');
app.use(express.static('public'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(session({
  resave: true,
  saveUninitialized: true,
  secret: config.secretKey
}));
app.use(flash());
app.use(validator({ customValidators }));
app.use(redirectBack);
app.use(currentUrl);
app.use(currentUrl);

app.use(mainRoute);
app.use(userRoute);
app.use(authMiddleware, adminRoute);

// Error handler

// app.use(errorLogger);
