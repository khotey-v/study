import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const Error = new Schema({
  message: String,
  createdAt: { type: Date, default: Date.now }
});

export default mongoose.model('Error', Error);