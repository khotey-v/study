import mongoose from 'mongoose';

import { encode } from '../helper/replace-html';

const Schema = mongoose.Schema;

const Topic = new Schema({
  title      : { type: String, index: true },
  url        : { type: String, index: true, unique: true },
  description: String,
  text       : String,
  createdAt  : { type: Date, default: Date.now }
});

Topic.pre('save', function(next) {
  const topic = this;

  topic.description = `${topic.description.substr(0, 100)}...`;
  topic.text = encode(topic.text);
  next();
});

Topic.pre('update', function(next) {
  const topic = this;

  topic.update({ description: topic._update.$set.description });
  topic.update({ text: encode(topic._update.$set.text) });

  next();
});

export default mongoose.model('Topic', Topic);
