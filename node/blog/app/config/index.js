const config = {
  database: 'localhost/blog',
  port: 3000,
  secretKey: 'secret-key'
};

export default config;
