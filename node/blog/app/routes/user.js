import express from 'express';

import User from '../models/user';

const router = express.Router();

router.post('/login', (req, res, next) => {
  const user = req.body;

  User.findOne({ login: user.login } , (err, _user) => {
    if (err) return next(err);

    if (!_user) {
      return res.redirect(req.backUrl);
    }

    if (_user.comparePassword(user.password)) {
      req.session.user = _user;

      res.redirect('/admin');
    }
  });
});

export default router;
