import express from 'express';
import elasticsearch from 'elasticsearch';

const client = new elasticsearch.Client({
  host: 'localhost:9200',
  log: 'trace'
});

const router = express.Router();

router.post('/search', (req, res, next) => {
  const query = req.body.query;

  client.search({
    q: ``
  }).then(function (body) {
    var hits = body.hits.hits;

    res.json(hits);
  }, function (error) {
    next(error);
  });

});

export default router;
