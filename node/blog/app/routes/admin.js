import express from 'express';

import Topic from '../models/topic';
import { decode } from '../helper/replace-html';

const router = express.Router();

router.get('/admin', (req, res) => {
  res.redirect('/admin/topics');
});

router.get('/admin/topics', (req, res, next) => {
  Topic.find({}, (err, topics) => {
    if (err) return next(err);

    const length = topics.length;

    res.render('admin/topics/list', { length, topics });
  });
});

router.get('/admin/topics/update/:id', (req, res, next) => {
  const _id = req.params.id;

  Topic.findOne({ _id }, (err, topic) => {
    if (err) return next(err);

    topic.text = decode(topic.text);

    res.render('admin/topics/update', { topic });
  });
});

router.get('/admin/topics/create', (req, res) => {
  res.render('admin/topics/create');
});

// Create/Update

router.post('/admin/topics/create', (req, res, next) => {
  const topic = req.body;

  req.checkBody('title').validateTitle();
  req.checkBody('url').validateUrl();
  req.checkBody('text').validateText();

  const errors = req.validationErrors();

  if (errors) {
    return res.render('admin/topics/create', { errors, topic  });
  }

  topic.description = `${topic.text.substr(0, 70)}...`;

  Topic.create(topic, (err, topic) => {
    if (err) return next(err);

    res.redirect('/admin/topics');
  });
});

router.post('/admin/topics/update/:id', (req, res, next) => {
  const _id = req.params.id;
  const topic = req.body;

  req.checkBody('title').validateTitle();
  req.checkBody('url').validateUrl();
  req.checkBody('text').validateText();

  const errors = req.validationErrors();

  if (errors) {
    return res.render('admin/topics/update', { errors, topic  });
  }

  Topic.update({ _id }, topic, (err) => {
    if (err) return next(err);


    res.redirect('/admin/topics');
  });
});

// Delete

router.get('/admin/topics/delete/:id', (req, res) => {
  const _id = req.params.id;

  Topic.remove({ _id }, (err) => {
    if (err) return next(err);

    res.redirect(req.backUrl)
  })
});

export default router;
