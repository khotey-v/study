import express from 'express';

import Topic from '../models/topic';

const router = express.Router();

router.get('/', (req, res) => {

  Topic.find({}).sort({ createdAt: 'desc' }).exec((err, topics) => {
    const length = topics.length;

    res.render('client/topics/main', { length, topics });
  });
});

router.get('/topics/:url', (req, res, next) => {

  const url = req.params.url;

  if (!url) {
    return res.redirect('/not-found');
  }

  Topic.findOne({ url }, (err, topic) => {
    if (err) return next(err);

    res.render('client/topics/topic', { topic });
  });
});

export default router;
