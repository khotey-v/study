import escape from 'escape-html';

import { AllHtmlEntities } from 'html-entities';

let entities = new AllHtmlEntities;

let replaceAll = function(string, search, replacement) {
  return string.replace(new RegExp(search, 'g'), replacement);
};

export function encode(text) {
  text = escape(text);
  console.log(text);

  text = replaceAll(text, repReg.css, rep.css);
  text = replaceAll(text, repReg.js, rep.js);
  text = replaceAll(text, repReg.html, rep.html);
  text = replaceAll(text, repReg.end, rep.end);
  text = replaceAll(text, repReg.tab, rep.tab);

  return text;
}

export function decode(text) {
  text = replaceAll(text, rep.css, repReg.css);
  text = replaceAll(text, rep.js, repReg.js);
  text = replaceAll(text, rep.html, repReg.html);
  text = replaceAll(text, rep.end, repReg.end);
  text = replaceAll(text, rep.tab, repReg.tab);

  return entities.decode(text);
}

const rep = {
  css: defaultString('css'),
  js: defaultString('js'),
  html: defaultString('html'),
  end: '</pre></code>',
  tab: '<div class="tab"></div>'
};

const repReg = {
  css: '```css',
  js: '```js',
  html: '```html',
  end: '```',
  tab: '\t'
};

function defaultString(text) {
  if (!text) {
    return `\n</pre></code>`;
  }

  return `<pre><code class="${text}">\n`;
}
