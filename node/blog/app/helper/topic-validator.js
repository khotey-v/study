const topicValidator = {
  validateTitle:  value =>  {
    let title = value.trim();

    if (!title.length) {
      return false;
    }

    return true;
  },

  validateUrl: value => {
    let url = value.trim();

    if (!url.length) {
      return false;
    }

    return true;
  },

  validateText: value => {
    let text = value.trim();

    if (!text.length) {
      return false;
    }

    return true;
  }
};

export default topicValidator;
