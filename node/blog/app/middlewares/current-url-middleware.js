export default function (req, res, next) {
  req.currentUrl = req.path.split('/')[1];
  req.app.locals.currentUrl = req.currentUrl;

  next();
}
