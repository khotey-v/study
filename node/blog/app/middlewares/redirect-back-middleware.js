export default function (req, res, next) {
  const backUrl = req.header('Referer') || '/';

  req.backUrl = backUrl;
  req.app.locals.backUrl = backUrl;

  next();
}
