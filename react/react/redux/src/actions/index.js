export function selectBook(book) {

  // selectBook is an ActionCreator, it needs to return n actions.
  // an object with type property
  return {
    payload: book,
    type: 'BOOK_SELECT'
  };
}