import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { selectBook } from '../actions';

class BookList extends Component {

  render() {
    return (
      <ul className="list-group col-sm-4">
        { this.renderList() }
      </ul>
    );
  }

  renderList() {
    return this.props.books.map(book => {
      return (
        <li 
          key={book.title} 
          className="list-group-item"
          onClick={() => this.props.selectBook(book)}>
          {book.title}
        </li>
      );
    });
  }
}

// what data get from store(state)
function mapStateToProps(state) {
  return {
    books: state.books
  }
}

// Anything returned from this fun will end up as props
// on the BookList container
function mapDispatchToProps(dispatch) {

  // when selectBook is called, the result of function be passed
  // to all reducers
  return bindActionCreators({ selectBook }, dispatch);
}

// take data from state and put in BookList props, what data take = mapStateToProps
// take actions from mapDispatchToProps and set to props
export default connect(mapStateToProps, mapDispatchToProps)(BookList);