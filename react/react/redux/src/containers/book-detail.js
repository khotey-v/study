import React, { Component } from 'react';
import { connect } from 'react-redux';

class BookDetail extends Component {

  render() {
    const {book} = this.props;

    if (!book) {
      return <h1>No Selected book</h1>;
    }

    return (
      <div>
        <h1>Details for: {book.title}</h1>
        <p>
          Pages: {book.pages}
        </p>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    book: state.activeBook
  };
}

export default connect(mapStateToProps)(BookDetail);