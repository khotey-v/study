export default function () {
  return [
    {title: 'Javascript', pages: 12},
    {title: 'Harry Potter', pages: 323},
    {title: 'Eloquent Ruby', pages: 4334}
  ];
};