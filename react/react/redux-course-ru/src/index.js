import 'babel-polyfill';
import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import configureStore from './store/configureStore';
import './styles/app.css';

const store = configureStore();

import App from './containers/app';

render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.querySelector('#root')
)