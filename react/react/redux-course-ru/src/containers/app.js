import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux'
import User from '../components/user';
import Page from '../components/page';
import * as pageActions from '../actions/page-actions';

class App extends Component {

  render() {
    const { user, page } = this.props;
    const { setYear, getPhotos } = this.props.pageActions;

    return (
      <div>
        <User user={user} />
        <Page
          page={page}
          setYear={setYear}
          getPhotos={getPhotos}
          fetching={page.fetching} />
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    user: state.user,
    page: state.page
  };
}

function mapDispatchToProps(dispatch) {
  return {
    pageActions: bindActionCreators(pageActions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(App);