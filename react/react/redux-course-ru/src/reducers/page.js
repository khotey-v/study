import {
  SET_YEAR,
  GET_PHOTOS_REQUEST,
  GET_PHOTOS_SUCCESS } from '../constants/page';

const initState = {
  year: 2016,
  photos: []
};

export default function page(state = initState, action) {

  console.log('page reducer');
  switch(action.type) {

    case SET_YEAR:
      return { ...state, year: action.payload };

    case GET_PHOTOS_REQUEST:
      return { ...state, year: action.payload, fetching: true }

    case GET_PHOTOS_SUCCESS:
      return { ...state, photos: action.payload, fetching: false }
  }

  return state;
}