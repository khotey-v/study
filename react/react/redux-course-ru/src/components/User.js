import React, { PropTypes, Component } from 'react';

export default class User extends Component {

  render() {
    const { name } = this.props.user;

    return (
      <div className='ib user'>
        <p>
          Привет, { name } !
        </p>
      </div>
    );
  }
}

User.propsTypes = {
  name: PropTypes.string.isRequired
};