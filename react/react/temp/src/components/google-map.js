import React from 'react';
import { GoogleMapLoader, GoogleMap, Marker } from 'react-google-maps';

export default (props) => {
  return (
    <section style={{height: '100%'}}>
      <GoogleMapLoader
        containerElement={ <div style={{ height: '200px', width: '200px' }} /> }

        googleMapElement={
          <GoogleMap
            defaultZoom={11}
            defaultCenter={{ lat: props.lat, lng: props.lon }}>
          </GoogleMap>
        }
      />
    </section>
  );
}