import React, { Component  } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { fetchWeather } from '../actions';

class SearchBar extends Component {

  state = { term: '' };

  constructor(props) {
    super(props);
  }

  onInputChange(e) {
    let term = e.target.value;

    this.setState({ term });
  }

  onFormSubmit(e) {
    e.preventDefault();

    this.props.fetchWeather(this.state.term);
    this.setState({ term: '' });
  }

  render() {
    return (
      <div>
        <form className='input-group search-bar' onSubmit={ ::this.onFormSubmit }>
          <input
            className="form-control"
            placeholder='Enter City'
            value={ this.state.term }
            onChange={ ::this.onInputChange }
          />
          <span className='input-group-btn'>
            <button type='submit' className='btn btn-secondary search-bar__submit-button'>Submit</button>
          </span>
        </form>
      </div>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ fetchWeather }, dispatch);
}

export default connect(null, mapDispatchToProps)(SearchBar);