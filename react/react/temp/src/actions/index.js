import axios from 'axios';

const API_KEY = '20de0d7f11a528348a6741900d92babb';
const ROOT_URL = `http://api.openweathermap.org/data/2.5/forecast?&mode=json&appid=${API_KEY}`;

export const FETCH_WEATHER = 'FETCH_WEATHER';

export async function fetchWeather(city) {
  const url = `${ROOT_URL}&q=${city}`;

  const weather = await axios(url);

  return {
    type: FETCH_WEATHER,
    payload: weather.data
  };
}