import React, {Component} from 'react';

class SearchBar extends Component {

  constructor(props) {
    super(props);

    this.state = {term: ''};
  }

  render() {
    return (
      <input type="text"
             className="form-control"
             id="search"
             placeholder="Search.."
             onChange={ e => this.onInputChange(e.target.value) }
      />
    );
  }

  onInputChange(term) {
    this.props.onSearchVideos(term);
  }
}

export default SearchBar;
