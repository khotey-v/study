import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import YS from 'youtube-api-search';
import _ from 'lodash';

import SearchBar from './components/search-bar';
import VideoList from './components/video-list';
import VideoDetails from './components/video-detail';

const API_KEY = 'AIzaSyDZCOnQUjT9IMEesbwTMvY5LzSop8SVB7A';

class App extends Component {

  constructor(props) {
    super(props);

    this.state = {
      videos: [],
      selectedVideo: null
    };

    this.searchVideo('javascript');
  }


  selectVideo(selectedVideo) {
    this.setState({ selectedVideo });
  }

  searchVideo(term) {
    YS({ key: API_KEY, term }, videos => {
      this.setState({ videos, selectedVideo: videos[0] });
    });
  }

  render() {

    // call function only after 300ms after prev call
    const videoSearch = _.debounce(term => this.searchVideo(term), 300);

    return (
      <div>
        <SearchBar onSearchVideos={ videoSearch }/>
        <VideoDetails video={ this.state.selectedVideo } />
        <VideoList
          videos={ this.state.videos }
          onVideoSelect={ selectedVideo => this.selectVideo(selectedVideo) }
        />
      </div>
    );
  }
}

ReactDOM.render(
  <App />,
  document.querySelector('.container')
);
