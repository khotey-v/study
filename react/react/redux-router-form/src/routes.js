import React from 'react';
import { Router, IndexRoute } from 'react-router';

import App from './components/app';
import PostsIndex from './components/posts-index';
import PostsNew from './components/posts-new';
import PostsShow from './components/posts-show';

const Greeting = () => {
  return <div> Hello! </div>;
}

export default (
  <Router path='/' component={App}>
    <IndexRoute component={PostsIndex} />
    <Router path='posts/new' component={PostsNew} />
    <Router path='posts/:id' component={PostsShow} />
  </Router>
);
