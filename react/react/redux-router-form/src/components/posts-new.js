import React, { Component, PropTypes } from 'react';
import { reduxForm } from 'redux-form';
import { Link } from 'react-router';

import { createPost  } from '../actions'

class PostsNew extends Component {

  static contextTypes = {
    router: PropTypes.object
  };

  onSubmit(props) {
    const result = this.props.createPost(props);

    result
      .then(data => {
        // post successed created, navigate to /
        this.context.router.push('/');
      });
  }

  render() {

    // handleSubmit called when user submit form
    const { fields: { title, categories, content }, handleSubmit } = this.props;

    // ...title = fromProps={title}
    return (
      <div>
        <form onSubmit={ handleSubmit(::this.onSubmit) } autoComplete='off' >
          <h3>Create new post</h3>

          <div className={`form-group ${title.touched && title.invalid ? 'has-danger' : ''}`}>
            <label>Title</label>
            <input type='text' className='form-control' {...title} />
            <div className='text-help'>
              {title.touched ? title.error : ''}
            </div>
          </div>

          <div className={`form-group ${categories.touched && categories.invalid ? 'has-danger' : ''}`}>
            <label>Categories</label>
            <input type='text' className='form-control' {...categories} />
            <div className='text-help'>
              {categories.touched ? categories.error: ''}
            </div>
          </div>

          <div className={`form-group ${content.touched && content.invalid ? 'has-danger' : ''}`}>
            <label>Content</label>
            <textarea type='text' className='form-control' {...content} />
            <div className='text-help'>
              {content.touched ? content.error : ''}
            </div>
          </div>

          <button type='submit' className='btn btn-primary'>Submit</button>
          <Link to='/' className='btn btn-danger'>
            Cencel
          </Link>

        </form>
      </div>
    );
  }
}

function validate(values) {
  const errors = {};

  if (!values.title) {
    errors.title = 'Enter title of the post';
  }

  if (!values.categories) {
    errors.categories = 'Enter categories of the post';
  }

  if (!values.content) {
    errors.content = 'Enter content of the post';
  }

  return errors;
}

// connect: 1st arg mapStateToProps, 2nd mapDispatchToProps
// reduxForm: 1st arg config, 2st arg mapStateToProps, 3nd mapDispatchToProps

// config for validation
export default reduxForm({
  form: 'PostsNewForm',
  fields: ['title', 'categories', 'content'],
  validate
}, null, { createPost })(PostsNew);