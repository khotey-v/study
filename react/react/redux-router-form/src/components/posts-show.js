import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';

import { fetchPost, deletePost } from '../actions';

class PostsShow extends Component {

  postId = this.props.params.id;

  static contextTypes = {
    router: PropTypes.object
  };

  componentWillMount() {
    this.props.fetchPost(this.postId);
  }

  onDeleteClick() {
    const result = this.props.deletePost(this.props.post.id);

    result.then(data => {
      this.context.router.push('/');
    });
  }

  render() {
    const { post } = this.props;

    if (!post) {
      return <div>Loading...</div>;
    }

    return (
      <div>
        <Link to='/'>Back to posts list</Link>
        <button className='btn btn-danger pull-xs-right' onClick={ ::this.onDeleteClick }>Delete</button>
        <h3>{ post.title }</h3>
        <h6>Categories: { post.title }</h6>
        <p>
          { post.content }
        </p>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    post: state.posts.post
  }
}

export default connect(mapStateToProps, { fetchPost, deletePost })(PostsShow);