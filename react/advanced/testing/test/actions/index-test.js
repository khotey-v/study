import { expect } from '../test_helper';

import { SAVE_COMMET } from '../../src/actions/types';
import { saveComment } from '../../src/actions';

describe('Actions', () => {
  const comment = 'new Comment';

  describe('saveComment', () => {
    it('has the correct type', () => {
      const action = saveComment(comment);

      expect(action.type).to.equal(SAVE_COMMET);
    });

    it('has the correct payload', () => {
      const action = saveComment(comment);

      expect(action.payload).to.equal(comment);
    });

  });

});