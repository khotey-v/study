import { renderComponent, expect} from '../test_helper';

import CommentList from '../../src/components/comment-list';

describe('CommentList', () => {

  let component;
  let comments;

  beforeEach(() => {
    comments = ['test comment', 'new comment'];
    component = renderComponent(CommentList, null, { comments: [...comments] });
  });

  it('shows an li for each comment', () => {
    expect(component.find('li').length).to.equal(comments.length);
  });

  it('shows each comment that is provided', () => {
    expect(component).to.contain(comments[0]);
    expect(component).to.contain(comments[1]);
  });

});