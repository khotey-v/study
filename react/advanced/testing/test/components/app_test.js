import { renderComponent, expect } from '../test_helper';
import App from '../../src/components/app';
import CommentBox from '../../src/components/comment-box';

describe('App', () => {
  let componentApp;
  let componentCommentBox;

  beforeEach(() => {
    componentApp = renderComponent(App);
    componentCommentBox = renderComponent(CommentBox);
  })

  it('shows a comment box', () => {
    expect(componentApp.find('.comment-box')).to.exist;
  });

  it('shows a comment list', () => {
    expect(componentApp.find('.comment-list')).to.exist;
  });

});
