import { renderComponent, expect } from '../test_helper';
import CommnentBox from '../../src/components/comment-box';

describe('ComponentBox', () => {
  let component;

  beforeEach(() => {
    component = renderComponent(CommnentBox);
  });

  it('has the correct class', () => {
    expect(component).to.have.class('comment-box');
  })

  it('has a text area', () => {
    expect(component.find('textarea')).to.exist;
  });

  it('has a button', () => {
    expect(component.find('button')).to.exist;
  });

  describe('entering some text', () => {
    const commentText = 'new comment';

    beforeEach(() => {
      component.find('textarea').simulate('change', commentText);
    });


    it('shows text that is entered', () => {
      expect(component.find('textarea')).to.have.value(commentText);
    });

    it('when submitted, clears the textarea', () => {
      component.simulate('submit');
      expect(component.find('textarea')).to.have.value('');
    });
  });

});