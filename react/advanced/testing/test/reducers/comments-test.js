import { expect } from '../test_helper';

import commentReducer from '../../src/reducers/comments';
import { SAVE_COMMET } from '../../src/actions/types';
import { saveComment } from '../../src/actions';

describe('Comments Reducer', () => {
  it('handles action with unknown type', () => {
    expect(commentReducer(undefined, {})).to.be.instanceof(Array);
  });

  it('handles action of type SAVE_COMMENT', () => {
    const action = saveComment('new Comment');

    expect(commentReducer([], action)).to.eql(['new Comment']);
  });
});