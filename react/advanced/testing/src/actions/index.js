import { SAVE_COMMET } from './types';

export function saveComment(commet) {
  return {
    type: SAVE_COMMET,
    payload: commet
  };
};