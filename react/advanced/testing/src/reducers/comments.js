import { SAVE_COMMET } from '../actions/types';

export default function (state = [], action) {
  switch(action.type) {
    case SAVE_COMMET:
      return [...state, action.payload];
  }

  return state;
}