import { Router } from 'express';
import passport from 'passport';

import Authentication from '../controllers/authentication';
import passportService from '../services/passport';

const router = Router();
const requireAuth = passport.authenticate('jwt', { session: false });
const requireSignin = passport.authenticate('local', { session: false });

router.get('/secret', requireAuth);
router.post('/signup', Authentication.signup);
router.post('/signin', requireSignin, Authentication.signin);

export default router;