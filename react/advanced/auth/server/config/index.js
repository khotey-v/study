const conf = {
  port: 3000,
  mongo: 'localhost/auth',
  secret: 'secret-string'
};

export default conf;