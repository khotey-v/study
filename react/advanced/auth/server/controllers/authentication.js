import User from '../models/user';
import jwt from 'jwt-simple';

import config from '../config';

function tokenForUser(user) {
  const timestamp = new Date().getTime();

  return jwt.encode({ sub: user._id, iat: timestamp }, config.secret);
}

exports.signup = async (req, res, next) => {
  const { email, password } = req.body;

  if (!email || !password) {
    return res
      .status(422)
      .send({ error: 'You must provide email and password' });
  }

  let user = await User.findOne({ email });

  if (user) {
    return res
      .status(422)
      .send({ error: 'Email is in use'});
  }

  if (!user) {
    user = new User({ email, password });

    await user.save();
  }

  res.json({ token: tokenForUser(user) });
}

exports.signin = async (req, res, next) => {
  // user already had, just create a token
  const user = req.user // from done(null, user) from passport
  res.json({ token: tokenForUser(user) });
}