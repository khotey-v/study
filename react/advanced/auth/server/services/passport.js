import passport from 'passport';
import { Strategy, ExtractJwt } from 'passport-jwt';
import LocalStategy from 'passport-local';

import User from '../models/user';
import config from '../config';

// create local strategy
const localOption = {
  usernameField: 'email'
};

const localLogin = new LocalStategy(localOption, async (email, password, done) => {
  const user = await User.findOne({ email });

  if (!user) {
    return done(null, false);
  }

  user.comparePassword(password, function(err, isMatch) {
    if (err) { return done(err); }
    if (!isMatch) { return done(null, false); }

    return done(null, user);
  });
});

// create jwt strategy
const jwtOptions = {
  jwtFromRequest: ExtractJwt.fromHeader('authentication'),
  secretOrKey: config.secret
};

const jwtLogin = new Strategy(jwtOptions, async (payload, done) => {
  const id = payload.sub; // controllers/authentication tokenForUser method
  const user = await User.findById(id);

  if (!user) {
    return done(null, false);
  }

  done(null, user);
});

passport.use(jwtLogin);
passport.use(localLogin);