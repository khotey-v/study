import express from 'express';
import bodyParser from 'body-parser';
import morgan from 'morgan';
import mongoose from 'mongoose';
import bluebird from 'bluebird';
import cors from 'cors';

import config from './config';
import router from './routes';

const PORT = process.env.PORT || config.port;
const app = express();

mongoose.Promise = bluebird;

mongoose.connect(config.mongo, (err) => {
  if (err) throw err;
});

app.use(morgan('combined')); // logger
app.use(bodyParser.json());
app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(router);

app.listen(PORT, (err) => {
  console.log(`Server listen port: ${PORT}`);
});