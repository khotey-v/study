import axios from 'axios';
import { browserHistory } from 'react-router';

import { AUTH_USER, AUTH_ERROR, UNAUTH_USER } from './types';

const ROOT_URL = 'http://localhost:3000';

export function signinUser({ email, password }) {

  // return function instead action object (redux-thunk middleware)
  // allows us to send many actions, not the one action
  return function(dispatch) {
    const request = axios.post(`${ROOT_URL}/signin`, { email, password });

    request
      .then((responce) => {
        dispatch({ type: AUTH_USER });
        localStorage.setItem('token', responce.data.token);
        browserHistory.push('/feature');
      })
      .catch((err) => {
        dispatch(authError('Bad login info'));
      });
  }
}

export function signoutUser() {
  localStorage.removeItem('token');

  return { type: UNAUTH_USER. payload: new Promise*() };
}

export function authError(error) {
  return {
    type: AUTH_ERROR,
    payload: error
  }
}