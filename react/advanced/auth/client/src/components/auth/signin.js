import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import { connect } from 'react-redux';

import * as actions from '../../actions';

class Signin extends Component {

  hanldeFormSubmit({ email, password }) {

    this.props.signinUser({ email, password });
  }

  renderAlert() {
    if (this.props.errorMessage) {
      return (
        <div className='alert alert-danger'>
          <strong>Oopos!</strong> {this.props.errorMessage}
        </div>
      );
    }
  }

  render() {
    const { handleSubmit } = this.props;

    return (
      <form onSubmit={handleSubmit(::this.hanldeFormSubmit)}>
        <fieldset className='form-group'>
          <label>
            Email:
          </label>
          <Field name="email" component="input" type="text" className='form-control'/>
        </fieldset>
        <fieldset className='form-group'>
          <label>
            Password:
          </label>
          <Field name="password" component="input" type="password" className='form-control'/>
        </fieldset>
        {this.renderAlert()}
        <button className='btn btn-primary' action='submit'>Sign In</button>
      </form>
    );
  }
}

Signin = reduxForm({
  form: 'signin',
  fields: ['email', 'password']
})(Signin);

function mapStateToProps({ auth }) {
  return { errorMessage: auth.error };
}

export default connect(mapStateToProps, actions)(Signin);