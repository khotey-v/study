export default function({ dispatch }) {
  return next => action => {

    if (!action.payload.then || !action.payload) {
      // go to next middlewares
      return next(action);
    }

    action.payload
      .then(responce => {
        const newAction = { ...action, payload: responce };

        // go to all middlewares again from start
        dispatch(newAction);
      });
  };
}

// return function (next) {
//   return function (action) {

//   }
// }