import React, { Component } from 'react';

export default () => {
  return (
    <div>
      Super Special Secret Recipe
      <ul>
        <li>1 cup Sugar</li>
        <li>1 cup salt</li>
        <li>200 g meat</li>
      </ul>
    </div>
  );
}