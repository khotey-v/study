import express from 'express';
import User from '../models/user';

const router = express.Router();

router.get('/users', (req, res) => {
  res.json(User.getAll());
});

router.post('/users', (req, res) => {
  const user = req.body.data;

  res.json(User.add(user));
});

router.put('/users/:index', (req, res) => {
  const index = req.params.index;
  const updatedUser = req.body.data;

  console.log(index, updatedUser);

  res.json(User.update(index, updatedUser));
});

router.delete('/users/:index', (req, res) => {
  const index = req.params.index;

  res.json(User.delete(index));
});

export default router;
