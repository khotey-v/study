export default class User {

  constructor($http) {
    this.$http = $http;
  }

  getAll() {
    return this.$http.get('/api/users');
  }

  add(user) {
    return this.$http.post('/api/users', { data: user });
  }

  update(index, user) {
    return this.$http.put(`/api/users/${index}`, { data: user });
  }

  delete(index) {
    return this.$http.delete(`/api/users/${index}`);
  }
}

User.$inject = ['$http'];
