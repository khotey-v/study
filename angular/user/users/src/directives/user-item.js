export default class UserItem {

  constructor() {
    this.scope = { user: '=', users: '='};
    this.restrict = 'EA';
    this.replace = true;
    this.controllerAs = 'UserItemCtrl';
    this.controller = UserItemCtrl;
    this.templateUrl = '/templates/user-item.html';
    this.bindToController = true;
  }
};

class UserItemCtrl {

  constructor(UserService) {
    this.UserService = UserService;
  }
}

UserItemCtrl.$inject = ['UserService'];
