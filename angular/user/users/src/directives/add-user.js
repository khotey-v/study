export default class AddUser {

  constructor() {
    this.scope = true;
    this.restrict = 'E';
    this.replace = true;
    this.controllerAs = 'AddUserCtrl';
    this.controller = AddUserCtrl;
    this.template = '/templates/user-item.html';
  }
};

class AddUserCtrl {

  constructor(UserService) {

  }
}

UserItemCtrl.$inject = ['UserService'];
