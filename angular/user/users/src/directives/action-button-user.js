export default class ActionButtonUser {

  constructor() {
    this.scope = { user: '=', users: '='};
    this.restrict = 'E';
    this.replace = true;
    this.controllerAs = 'ActionButtonUserCtrl';
    this.controller = ActionButtonUserCtrl;
    this.templateUrl = '/templates/action-button-user.html';
    this.bindToController = true;
  }
};

class ActionButtonUserCtrl {

  constructor(UserService, ngDialog) {
    this.UserService = UserService;
    this.ngDialog = ngDialog;
  }

  delete() {
    const index = this.user.index;

    this.UserService
      .delete(index)
      .then((data) => {
        this.users.splice(index, 1);
      });
  }

  update() {
    const user = this.user;
    const users = this.users;
    const index = this.user.index;

    this.ngDialog.open({
      template: '/templates/update-user.html',
      controller: ['$scope', 'UserService', ($scope, UserService) => {
        $scope.user = Object.assign({
          name: '',
          email: '',
          phone: '',
          address: '',
          street: '',
          city: '',
          state: ''
        }, user);

        $scope.update = function() {
          UserService
            .update(index, $scope.user)
            .then((data) => {
              console.log($scope.user)
              users[index] = $scope.user;
            });
        }
      }]
    });
  }
}

ActionButtonUserCtrl.$inject = ['UserService', 'ngDialog'];
