export default class UserList {

  constructor() {
    this.scope = true;
    this.restrict = 'E';
    this.replace = true;
    this.controllerAs = 'UserListCtrl';
    this.controller = UserListCtrl;
    this.templateUrl = '/templates/user-list.html';
  }
};

class UserListCtrl {

  constructor($scope, UserService) {
    this.UserService = UserService;
    this.$scope = $scope;
    this.users = [];

    this.init();
  }

  async init() {
    const data = await this.UserService.getAll();

    this.users = data.data;
  }

  addUser() {
    const user = this.$scope.user;

    this.UserService
    .add(user)
    .then((data) => {
      this.users.push(data.data);
      this.$scope.user = {};
    });

  }
}

UserListCtrl.$inject = ['$scope', 'UserService'];
