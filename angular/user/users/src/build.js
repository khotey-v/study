import angular from 'angular';
import ngDialog from 'ng-dialog';
import UserService from './services/user';
import UserList from './directives/user-list';
import UserItem from './directives/user-item';
import ActionButtonUser from './directives/action-button-user';

import './media/style.css';

var app = angular.module('User', [ngDialog]);

app
  .service('UserService', UserService)
  .directive('userList', () => new UserList)
  .directive('userItem', () => new UserItem)
  .directive('actionButtonUser', () => new ActionButtonUser);


// stage-2
// class Test {
//   a = 5;
//   static g = 8;

//   getA() {
//     return this.a;
//   }

//   getG() {
//     return Test.g;
//   }
// }

// let a = {k:5, h:7};
// let b = {...a};

// console.log((new Test).getG());


