class User {

  constructor() {
    this.users = [];
  }

  add(user) {
    let userCount = this.users.length;
    user.index = userCount;
    const newUser = new UserObj(user);

    this.users.push(newUser);

    return newUser;
  }

  delete(index) {
    this.users.splice(index, 1);
  }

  update(index, user) {
    const updatedUser = new UserObj(user)
    this.users[index] = updatedUser;

    return updatedUser;
  }

  getAll() {
    return this.users;
  }

  get(index) {
    return this.users[index];
  }
}

function UserObj({
  name,
  email,
  phone,
  address,
  street,
  city,
  state,
  index
}) {
  this.name = name;
  this.email = email;
  this.phone = phone;
  this.address = address;
  this.street = street;
  this.city = city;
  this.state = state;
  this.index = index;
};

export default new User;
