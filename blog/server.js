import express from 'express';
import mongoose from 'mongoose';
import session from 'express-session';
import morgan from 'morgan';
import bodyParser from 'body-parser';

import config from './config';

const app = express();

app.use(bodyParser.json());
app.use(morgan('combined'));
app.use(session({
	secret: config.secret
}))

app.listen(config.port, (err) => {
	if (err) throw err;

	console.log(`Server listening port ${config.port}`);
});
